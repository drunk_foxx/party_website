# frozen_string_literal: true

environment ENV.fetch('RAILS_ENV') { 'development' }

port ENV['PORT']

workers ENV['WEB_CONCURRENCY']
threads ENV['RAILS_MIN_THREADS'], ENV['RAILS_MAX_THREADS']
preload_app!

before_fork do
  ActiveRecord::Base.connection_pool.disconnect! if defined?(ActiveRecord)
end

on_worker_boot do
  ActiveRecord::Base.establish_connection if defined?(ActiveRecord)
end

plugin :tmp_restart
