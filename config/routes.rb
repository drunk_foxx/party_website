# frozen_string_literal: true

Rails.application.routes.draw do
  root 'pages#party'

  resources :publications, only: %i[index show]
  resources :membership_requests, only: %i[new create]

  get :party, to: 'pages#party'
  get :libertarianism, to: 'pages#libertarianism'
  get :join, to: 'membership_requests#new'
  get :donate, to: 'pages#donate'

  get :platform, to: 'pages#platform'
  get :press, to: 'pages#press'
  get :success, to: 'pages#success'

  get :login, to: 'admin/sessions#new'
  post :logout, to: 'admin/sessions#destroy'

  namespace :admin do
    root 'pages#home'

    resources :publications, except: :show
    resources :users, except: :show do
      resource :password, only: %i[edit update]
    end
    resources :invites, except: %i[edit update]

    resources :sessions, only: %i[new create destroy]
    resources :password_resets, only: %i[new create]
  end
end
