import React from 'react'
import { shallow } from 'enzyme'

import { FooterParagraph } from './Footer-Paragraph'

describe('Footer-Paragraph', () => {
  const footerParagraph = shallow(<FooterParagraph />)

  it('is <p>', () => {
    expect(footerParagraph.is('p')).toBe(true)
  })

  it('has class Footer-Paragraph', () => {
    expect(footerParagraph.prop('className')).toBe('Footer-Paragraph')
  })

  it('is empty by default', () => {
    expect(footerParagraph.children().length).toBe(0)
  })
})

describe('Footer-Paragraph with children', () => {
  const footerParagraph = shallow(<FooterParagraph>Text node</FooterParagraph>)

  it('is not empty anymore', () => {
    expect(footerParagraph.children().length).toBe(1)
    expect(footerParagraph.text()).toBe('Text node')
  })
})
