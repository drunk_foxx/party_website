import React from 'react'
import { shallow } from 'enzyme'

import Button from '../../Button'
import { BaseFooterChatLink as FooterChatLink } from './Footer-ChatLink'

describe('Footer-ChatLink', () => {
  const footerChatLink = shallow(<FooterChatLink />)

  it('is Button', () => {
    expect(footerChatLink.is(Button)).toBe(true)
  })

  it('by default has class Footer-ChatLink', () => {
    expect(footerChatLink.prop('className')).toBe('Footer-ChatLink')
  })

  it('contains no children by default', () => {
    expect(footerChatLink.children().length).toBe(0)
  })

  it('has theme="telegram"', () => {
    expect(footerChatLink.prop('theme')).toBe('telegram')
  })

  it('links to tg://resolve?domain=lpcht', () => {
    expect(footerChatLink.prop('uri')).toBe('tg://resolve?domain=lpcht')
  })
})

describe('Footer-ChatLink with other classes', () => {
  const footerChatLink = shallow(<FooterChatLink className='A B C' />)

  it('appends them to the existing class', () => {
    expect(footerChatLink.prop('className')).toBe('Footer-ChatLink A B C')
  })
})

describe('Footer-ChatLink with a child', () => {
  const footerChatLink = shallow(<FooterChatLink>Some child</FooterChatLink>)

  it('has that child inside', () => {
    expect(footerChatLink.matchesElement(<Button>Some child</Button>))
      .toBe(true)
  })
})
