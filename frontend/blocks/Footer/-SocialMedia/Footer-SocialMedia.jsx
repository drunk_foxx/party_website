import React from 'react'

import Social from '../../Social'
import { cnFooter } from '../Footer'
import './Footer-SocialMedia.css'

export const FooterSocialMedia = () => (
  <ul className={cnFooter('SocialMedia')}>
    <li>
      <Social
        medium='telegram'
        size='m'
        uri='tg://resolve?domain=Libertarians' />
    </li>
    <li>
      <Social
        medium='vk'
        size='m'
        uri='https://vk.com/lpr_public' />
    </li>
    <li>
      <Social
        medium='youtube'
        size='m'
        uri='https://www.youtube.com/channel/UCqPo0qIRA99S4Jz8thoBmng' />
    </li>
    <li>
      <Social
        medium='twitter'
        size='m'
        uri='https://twitter.com/libertarian_rus' />
    </li>
    <li>
      <Social
        medium='instagram'
        size='m'
        uri='https://www.instagram.com/libertarian_rus' />
    </li>
  </ul>
)
