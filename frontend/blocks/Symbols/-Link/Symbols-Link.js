import React from 'react'

import Link from '../../Link'
import { cnSymbols } from '../Symbols'
import './Symbols-Link.css'

export const SymbolsLink = (props) => {
  const newProps = Object.assign({}, props)
  newProps.className = cnSymbols('Link')
  newProps.theme = 'default'

  return <Link {...newProps} />
}
