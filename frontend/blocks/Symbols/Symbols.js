import React from 'react'
import { cn } from '@bem-react/classname'

import { SymbolsFormats } from './-Formats/Symbols-Formats'
import { SymbolsLink } from './-Link/Symbols-Link'
import { SymbolsLogo } from './-Logo/Symbols-Logo'
import png from './images/logo.png'
import svg from './images/logo.svg'
import './Symbols.css'

export const cnSymbols = cn('Symbols')

export const Symbols = (props) => (
  <div className={cnSymbols(null, [props.className])}>
    <SymbolsLogo />
    <SymbolsFormats>
      <SymbolsLink
        href={svg}
        download='Логотип Либертарианской партии.svg'>
        Логотип.svg
      </SymbolsLink>
      <SymbolsLink
        href={png}
        download='Логотип Либертарианской партии.png'>
        Логотип.png
      </SymbolsLink>
    </SymbolsFormats>
  </div>
)
