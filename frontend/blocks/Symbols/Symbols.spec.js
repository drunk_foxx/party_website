import React from 'react'
import { shallow } from 'enzyme'

import { SymbolsLogo } from './-Logo/Symbols-Logo'
import { Symbols } from './Symbols'

describe('Symbols', () => {
  const symbols = shallow(<Symbols />)

  it('is <div>', () => {
    expect(symbols.is('div')).toBe(true)
  })

  it('has class Symbols', () => {
    expect(symbols.prop('className')).toBe('Symbols')
  })

  it('contains Symbols-Logo', () => {
    expect(symbols.contains(<SymbolsLogo />)).toBe(true)
  })
})

describe('Symbols with one more class', () => {
  const symbols = shallow(<Symbols className='One-More' />)

  it('has one more class now', () => {
    expect(symbols.prop('className')).toBe('Symbols One-More')
  })
})
