import React from 'react'
import { shallow } from 'enzyme'

import { SymbolsFormats } from './Symbols-Formats'

describe('Symbols-Formats', () => {
  const symbolsFormats = shallow(<SymbolsFormats />)

  it('is <div>', () => {
    expect(symbolsFormats.is('div')).toBe(true)
  })

  it('has class Symbols-Formats', () => {
    expect(symbolsFormats.prop('className')).toBe('Symbols-Formats')
  })

  it('has no children by default', () => {
    expect(symbolsFormats.children().length).toBe(0)
  })
})

describe('Symbols-Formats with children', () => {
  const symbolsFormats = shallow(
    <SymbolsFormats>
      <a href='x.png' />
      <a href='x.svg' />
    </SymbolsFormats>
  )

  it('has children now', () => {
    expect(symbolsFormats.children().length).toBe(2)
  })
})
