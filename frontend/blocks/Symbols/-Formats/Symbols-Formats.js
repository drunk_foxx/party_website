import React from 'react'

import { cnSymbols } from '../Symbols'
import './Symbols-Formats.css'

export const SymbolsFormats = (props) => (
  <div className={cnSymbols('Formats')}>
    {props.children}
  </div>
)
