import React from 'react'
import { shallow } from 'enzyme'

import { Paragraph } from './Paragraph'

describe('Paragraph', () => {
  const paragraph = shallow(<Paragraph />)

  it('is <p> by default', () => {
    expect(paragraph.is('p')).toBe(true)
  })

  it('has undefined tag by default', () => {
    expect(paragraph.prop('tag')).toBeUndefined()
  })

  it('has class Paragraph by default', () => {
    expect(paragraph.prop('className')).toBe('Paragraph')
  })

  it('has no children', () => {
    expect(paragraph.children().length).toBe(0)
  })
})

describe('Paragraph with different tag', () => {
  const paragraph = shallow(<Paragraph tag='span' />)

  it('has different tag', () => {
    expect(paragraph.is('span')).toBe(true)
  })
})

describe('Paragraph with additional class', () => {
  const paragraph = shallow(<Paragraph className='Mixed-Class' />)

  it('includes additional class', () => {
    expect(paragraph.prop('className')).toBe('Paragraph Mixed-Class')
  })
})

describe('Paragraph with children', () => {
  const paragraph = shallow(<Paragraph>Here goes some text</Paragraph>)

  it('contains provided children', () => {
    expect(paragraph.children().length).toBe(1)
    expect(paragraph.text()).toBe('Here goes some text')
  })
})
