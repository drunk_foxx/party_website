import { withBemMod } from '@bem-react/core'

import { cnParagraph } from '../Paragraph'
import './Paragraph_theme_default.css'

export const ParagraphThemeDefault = withBemMod(
  cnParagraph(),
  { theme: 'default' }
)
