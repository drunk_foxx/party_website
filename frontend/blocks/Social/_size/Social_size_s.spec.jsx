import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialSizeS } from './Social_size_s'

const Social = SocialSizeS(BaseSocial)

describe('Social_size_s', () => {
  const socialSizeS = shallow(<Social size='s' />)

  it('has classes Social and Social_size_s', () => {
    expect(socialSizeS.prop('className')).toBe('Social Social_size_s')
  })
})
