import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonIcon } from '../-Icon/Button-Icon'
import { ButtonText } from '../-Text/Button-Text'
import { ButtonTypeLink } from './Button_type_link'

const Button = ButtonTypeLink(BaseButton)

describe('Button_type_link', () => {
  const buttonTypeLink = shallow(<Button type='link' />)

  it('is <a>', () => {
    expect(buttonTypeLink.is('a')).toBe(true)
  })

  it('has classes Button and Button_type_link by default', () => {
    expect(buttonTypeLink.prop('className')).toBe('Button Button_type_link')
  })

  it('does not have an href by default', () => {
    expect(buttonTypeLink.prop('href')).toBeUndefined()
  })

  it('by default has no Button-Icon', () => {
    expect(buttonTypeLink.exists('.Button-Icon')).toBe(false)
  })

  it('contains an empty Button-Text by default', () => {
    expect(buttonTypeLink.contains(<ButtonText />)).toBe(true)
  })
})

describe('Button_type_link with extra classes', () => {
  const buttonTypeLink = shallow(<Button type='link' className='Hey' />)

  it('appends them to <a>', () => {
    expect(buttonTypeLink.prop('className'))
      .toBe('Button Button_type_link Hey')
  })
})

describe('Button_type_link with URI', () => {
  const buttonTypeLink = shallow(
    <Button type='link' uri='https://domain.example' />
  )

  it('has it as href', () => {
    expect(buttonTypeLink.prop('href')).toBe('https://domain.example')
  })
})

describe('Button_type_link with an icon', () => {
  const buttonTypeLink = shallow(<Button type='link' icon={<svg />} />)

  it('passes that to Button-Icon', () => {
    expect(buttonTypeLink.contains(<ButtonIcon><svg /></ButtonIcon>))
      .toBe(true)
  })
})

describe('Button_type_link with text', () => {
  const buttonTypeLink = shallow(<Button type='link'>hello</Button>)

  it('passes them to the underlying Button-Text', () => {
    expect(buttonTypeLink.contains(<ButtonText>hello</ButtonText>)).toBe(true)
  })
})
