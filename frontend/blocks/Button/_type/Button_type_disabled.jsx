import React from 'react'
import { withBemMod } from '@bem-react/core'

import { cnButton } from '../Button'
import { ButtonIcon } from '../-Icon/Button-Icon'
import { ButtonText } from '../-Text/Button-Text'
import './Button_type_disabled.css'

const newBody = (Base, props) => (
  <div className={props.className}>
    <ButtonIcon>{props.icon}</ButtonIcon>
    <ButtonText>{props.children}</ButtonText>
  </div>
)

export const ButtonTypeDisabled = withBemMod(
  cnButton(),
  { type: 'disabled' },
  newBody
)
