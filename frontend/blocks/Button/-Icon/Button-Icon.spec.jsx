import React from 'react'
import { shallow } from 'enzyme'

import { ButtonIcon } from './Button-Icon'

describe('Button-Icon', () => {
  const buttonIcon = shallow(<ButtonIcon />)

  it('renders nothing', () => {
    expect(buttonIcon.type()).toBe(null)
  })
})

describe('Button-Icon with children', () => {
  const buttonIcon = shallow(<ButtonIcon><svg /></ButtonIcon>)

  it('is <span>', () => {
    expect(buttonIcon.is('span')).toBe(true)
  })

  it('has class Button-Icon', () => {
    expect(buttonIcon.prop('className')).toBe('Button-Icon')
  })

  it('contains provided children', () => {
    expect(buttonIcon.contains(<svg />)).toBe(true)
  })
})
