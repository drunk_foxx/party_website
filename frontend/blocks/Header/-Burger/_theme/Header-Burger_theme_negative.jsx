import React from 'react'
import { withBemMod } from '@bem-react/core'

import { cnHeader } from '../../Header'
import { BurgerIcon } from './icons/Burger'

const newBody = (Base, props) => {
  const newProps = Object.assign({}, props)
  newProps.icon = <BurgerIcon color='#fff' opacity='0.7' />

  return <Base {...newProps} />
}

export const HeaderBurgerThemeNegative = withBemMod(
  cnHeader('Burger'),
  { theme: 'negative' },
  newBody
)
