import { withBemMod } from '@bem-react/core'

import { cnHeader } from '../../Header'
import './Header-Link_active.css'

export const HeaderLinkActive = withBemMod(
  cnHeader('Link'),
  { active: true }
)
