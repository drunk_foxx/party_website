import React from 'react'
import { shallow } from 'enzyme'

import Logo from '../Logo'
import Menu from '../Menu'
import { HeaderActions } from './-Actions/Header-Actions'
import { HeaderSections } from './-Sections/Header-Sections'
import { HeaderBurger } from './-Burger/Header-Burger'
import { Header } from './Header'

describe('Header', () => {
  const header = shallow(<Header />)

  it('is <header>', () => {
    expect(header.is('header')).toBe(true)
  })

  it('has class Header by default', () => {
    expect(header.prop('className')).toBe('Header')
  })

  it('contains Logo with text "Либертарианская партия"', () => {
    const logo = <Logo type='link' uri='/'>Либертарианская партия</Logo>
    expect(header.contains(logo)).toBe(true)
  })

  it('includes Header-Actions with no predefined activeLink', () => {
    expect(header.contains(<HeaderActions />)).toBe(true)
  })

  it('has Header-Sections without any predefined activeLink', () => {
    expect(header.contains(<HeaderSections />)).toBe(true)
  })

  it('contains Header-Burger, passes handleBurgerClick as onClick', () => {
    const headerBurger = (
      <HeaderBurger onClick={header.instance().handleBurgerClick} />
    )
    expect(header.contains(headerBurger)).toBe(true)
  })

  it('opens Menu when handleBurgerClick is called', () => {
    const header = shallow(<Header />)
    header.instance().handleBurgerClick()
    expect(header.find(Menu).prop('closed')).toBe(false)
  })

  it('has closed Menu, passes handleMenuClose as onClose', () => {
    const menu = (
      <Menu onClose={header.instance().handleMenuClose} closed />
    )
    expect(header.contains(menu)).toBe(true)
  })

  it('closes Menu when handleMenuClose is called', () => {
    const header = shallow(<Header />)
    header.instance().handleBurgerClick()
    header.instance().handleMenuClose()
    expect(header.find(Menu).prop('closed')).toBe(true)
  })
})

describe('Header with more classes', () => {
  const header = shallow(<Header className='More Classes' />)

  it('appends provided classes to the default one', () => {
    expect(header.prop('className')).toBe('Header More Classes')
  })
})

describe('Header with burgerTheme', () => {
  const header = shallow(<Header burgerTheme='pretty' />)

  it('passes that as theme to Header-Burger', () => {
    expect(header.containsMatchingElement(<HeaderBurger theme='pretty' />))
      .toBe(true)
  })
})

describe('Header with logoTheme', () => {
  const header = shallow(<Header logoTheme='nice' />)

  it('passes that as theme to Logo', () => {
    const logo = <Logo theme='nice'>Либертарианская партия</Logo>
    expect(header.containsMatchingElement(logo)).toBe(true)
  })
})

describe('Header with activeLink', () => {
  const header = shallow(<Header activeLink='link' />)

  it('passes that as activeLink to Header-Actions', () => {
    const headerActions = <HeaderActions activeLink='link' />
    expect(header.containsMatchingElement(headerActions)).toBe(true)
  })

  it('passes that as activeLink to Header-Sections', () => {
    const headerSections = <HeaderSections activeLink='link' />
    expect(header.containsMatchingElement(headerSections)).toBe(true)
  })

  it('passes that activeLink as activeItem to Menu', () => {
    const menu = <Menu activeItem='link' />
    expect(header.containsMatchingElement(menu)).toBe(true)
  })
})
