import React from 'react'
import { shallow } from 'enzyme'

import { ContactLink } from '../-Link/Contact-Link'
import { ContactTelegram } from './Contact-Telegram'

describe('Contact-Telegram', () => {
  const contactTelegram = shallow(<ContactTelegram />)

  it('is <div>', () => {
    expect(contactTelegram.is('div')).toBe(true)
  })

  it('has class Contact-Telegram', () => {
    expect(contactTelegram.prop('className')).toBe('Contact-Telegram')
  })
})

describe('Contact-Telegram with children', () => {
  const contactTelegram = shallow(<ContactTelegram>telegram</ContactTelegram>)

  it('contains text "телеграм: "', () => {
    expect(contactTelegram.text()).toMatch('телеграм:\u00a0')
  })

  it('passes children to Contact-Link, adding @ to the beginning', () => {
    const contactLink = <ContactLink>@telegram</ContactLink>
    expect(contactTelegram.containsMatchingElement(contactLink)).toBe(true)
  })

  it('prepends children with schema, passes them to Contact-Link href', () => {
    expect(contactTelegram.find(ContactLink).prop('href'))
      .toBe('tg://resolve?domain=telegram')
  })
})
