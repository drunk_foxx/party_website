import React from 'react'
import { shallow } from 'enzyme'

import { ContactLink } from '../-Link/Contact-Link'
import { ContactPhone } from './Contact-Phone'

describe('Contact-Phone', () => {
  const contactPhone = shallow(<ContactPhone />)

  it('is Contact-Link', () => {
    expect(contactPhone.is(ContactLink)).toBe(true)
  })

  it('has class Contact-Phone', () => {
    expect(contactPhone.prop('className')).toBe('Contact-Phone')
  })

  it('by default got no children', () => {
    expect(contactPhone.children().length).toBe(0)
  })
})

describe('Contact-Phone with children', () => {
  const contactPhone = shallow(<ContactPhone>+7 123 456-78-90</ContactPhone>)

  it('has children', () => {
    expect(contactPhone.children().length).toBe(1)
    expect(contactPhone.render().text()).toBe('+7 123 456-78-90')
  })

  it('copies children to href, adding "tel:" to the beginning', () => {
    expect(contactPhone.prop('href')).toBe('tel:+7 123 456-78-90')
  })
})
