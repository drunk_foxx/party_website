import React from 'react'
import { shallow } from 'enzyme'

import { ContactDetails } from './Contact-Details'

describe('Contact-Details', () => {
  const contactDetails = shallow(<ContactDetails />)

  it('does not render anything by default', () => {
    expect(contactDetails.type()).toBe(null)
  })
})

describe('Contact-Details with false children', () => {
  const contactDetails = shallow(<ContactDetails>{false}</ContactDetails>)

  it('still does not render anything', () => {
    expect(contactDetails.type()).toBe(null)
  })
})

describe('Contact-Details with a single child', () => {
  const contactDetails = shallow(
    <ContactDetails>
      <a href='https://example.com' />
      {false}
    </ContactDetails>
  )

  it('is <address>', () => {
    expect(contactDetails.is('address')).toBe(true)
  })

  it('has class Contact-Details', () => {
    expect(contactDetails.prop('className')).toBe('Contact-Details')
  })

  it('contains the child', () => {
    expect(contactDetails.children().length).toBe(1)
    expect(contactDetails.contains(<a href='https://example.com' />))
      .toBe(true)
  })
})

describe('Contact-Details with multiple children', () => {
  const contactDetails = shallow(
    <ContactDetails>
      <a href='https://example.com' />
      <a href='mailto:email@address.example' />
    </ContactDetails>
  )

  it('includes these children', () => {
    expect(contactDetails.children().length).toBe(2)
  })
})
