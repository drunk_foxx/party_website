import React from 'react'
import { withBemMod } from '@bem-react/core'

import { cnLogo } from '../Logo'
import { LogoIcon } from '../-Icon/Logo-Icon'
import { LogoText } from '../-Text/Logo-Text'
import './Logo_type_link.css'

const newBody = (Base, props) => (
  <a className={props.className} href={props.uri}>
    <LogoIcon theme={props.iconTheme} />
    <LogoText>{props.children}</LogoText>
  </a>
)

export const LogoTypeLink = withBemMod(cnLogo(), { type: 'link' }, newBody)
