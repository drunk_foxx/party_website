import React from 'react'

import { cnLogo } from '../Logo'
import './Logo-Text.css'

export const LogoText = (props) => {
  if (React.Children.count(props.children) === 0) {
    return null
  }

  return (
    <span className={cnLogo('Text')}>
      {props.children}
    </span>
  )
}
