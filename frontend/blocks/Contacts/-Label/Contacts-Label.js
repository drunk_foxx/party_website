import React from 'react'

import { ContactsItem } from '../-Item/Contacts-Item'
import { cnContacts } from '../Contacts'
import './Contacts-Label.css'

export const ContactsLabel = (props) => {
  if (React.Children.toArray(props.children).length === 0) {
    return null
  }

  return (
    <ContactsItem className={cnContacts('Label')}>
      {props.children}
    </ContactsItem>
  )
}
