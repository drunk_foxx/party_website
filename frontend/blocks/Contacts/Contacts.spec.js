import React from 'react'
import { shallow } from 'enzyme'

import Heading from '../Heading'
import { ContactsDesc } from './-Desc/Contacts-Desc'
import { ContactsItem } from './-Item/Contacts-Item'
import { ContactsLabel } from './-Label/Contacts-Label'
import { Contacts } from './Contacts'

describe('Contacts', () => {
  const contacts = shallow(<Contacts />)

  it('is <div>', () => {
    expect(contacts.is('div')).toBe(true)
  })

  it('by default has class Contacts', () => {
    expect(contacts.prop('className')).toBe('Contacts')
  })

  it('does not contain Heading by default', () => {
    expect(contacts.find(Heading).length).toBe(0)
  })

  it('by default has no Contacts-Desc', () => {
    expect(contacts.find(ContactsDesc).length).toBe(0)
  })

  it('by default contains no Contacts-Item', () => {
    expect(contacts.find(ContactsItem).length).toBe(0)
  })

  it('contains blank Contacts-Label by default', () => {
    expect(contacts.contains(<ContactsLabel />)).toBe(true)
  })
})

describe('Contacts with extra classes', () => {
  const contacts = shallow(<Contacts className='Special-Contacts' />)

  it('has more classes now', () => {
    expect(contacts.prop('className'))
      .toBe('Contacts Special-Contacts')
  })
})

describe('Contacts with desc', () => {
  const contacts = shallow(<Contacts desc='Description...' />)

  it('has Contacts-Desc with that desc as children', () => {
    const contactsDesc = <ContactsDesc>Description...</ContactsDesc>
    expect(contacts.contains(contactsDesc)).toBe(true)
  })
})

describe('Contacts with title', () => {
  const contacts = shallow(<Contacts title='Some Title' />)

  it('contains Heading with corresponding text', () => {
    const heading = <Heading>Some Title</Heading>
    expect(contacts.containsMatchingElement(heading)).toBe(true)
  })
})

describe('Contacts with a child', () => {
  const contacts = shallow(<Contacts>a child</Contacts>)

  it('contains the child wrapped into Contact-Item', () => {
    expect(contacts.contains(<ContactsItem>a child</ContactsItem>)).toBe(true)
  })
})
