import { compose } from '@bem-react/core'

import { Page as BasePage } from './Page'
import {
  PageNameLibertarianism
} from './_name/_libertarianism/Page_name_libertarianism'
import { PageNamePlatform } from './_name/_platform/Page_name_platform'
import { PageNamePress } from './_name/_press/Page_name_press'

const Page = compose(
  PageNameLibertarianism,
  PageNamePlatform,
  PageNamePress
)(BasePage)

export default Page
