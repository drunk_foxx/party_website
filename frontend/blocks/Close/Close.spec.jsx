import React from 'react'
import { shallow } from 'enzyme'

import { Close } from './Close'

describe('Close', () => {
  const close = shallow(<Close />)

  it('is <button>', () => {
    expect(close.is('button')).toBe(true)
  })

  it('has class Close', () => {
    expect(close.prop('className')).toBe('Close')
  })

  it('has undefined onClick by default', () => {
    expect(close.prop('onClick')).toBeUndefined()
  })
})

describe('Close with onClick function', () => {
  const onClickFunc = jest.fn()
  const close = shallow(<Close onClick={onClickFunc} />)

  it('executes that function on click', () => {
    close.simulate('click')
    expect(onClickFunc).toBeCalled()
  })
})
