# frozen_string_literal: true

require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  def test_reset_password
    user = build(
      :user,
      id: '3317e507-628b-47e0-956a-dcd1594cd3c0',
      name: "User's name"
    )

    # Redefine token generation for predictable result.
    def user.generate_reset_password_token!
      self.reset_password_token = '9aboi_V7yUxbU9YtDkNg'
    end

    mail = UserMailer.reset_password(user)

    assert_emails 1 do
      mail.deliver_now
    end

    assert_equal ['notifications@new.libertarian-party.ru'], mail.from
    assert_equal [user.email], mail.to
    assert_equal I18n.t('user_mailer.reset_password.subject'), mail.subject

    assert_equal read_fixture('reset_password.txt').join, mail.body.to_s
  end
end
