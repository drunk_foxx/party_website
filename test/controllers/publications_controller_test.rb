# frozen_string_literal: true

require 'test_helper'

class PublicationControllerTest < ActionDispatch::IntegrationTest
  def test_index_without_any_publications
    get publications_url
    assert_response :success
  end

  def test_index_with_some_publications
    create(:article)
    create(:news)
    get publications_url
    assert_response :success
  end

  def test_show_article
    article = create(:article)
    get publication_url(article)
    assert_response :success
  end

  def test_show_news
    news = create(:news)
    get publication_url(news)
    assert_response :success
  end

  def test_show_statement
    statement = create(:statement)
    get publication_url(statement)
    assert_response :success
  end
end
