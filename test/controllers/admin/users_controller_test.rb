# frozen_string_literal: true

require 'test_helper'

module Admin
  class UsersControllerTest < ActionDispatch::IntegrationTest
    def setup
      @current_user = create(:user, password: 'password')
      login(email: @current_user.email, password: 'password')
    end

    def test_index
      get admin_users_url
      assert_forbidden
    end

    def test_new_without_invite
      get new_admin_user_url
      assert_response :bad_request
    end

    def test_new_with_nonexistent_invite
      params = {invite: 'nonexistent invite'}
      get new_admin_user_url, params: params
      assert_response :bad_request
    end

    def test_new_with_used_invite
      params = {invite: create(:user).invite_id}
      get new_admin_user_url, params: params
      assert_response :bad_request
    end

    def test_new_with_fresh_invite
      params = {invite: create(:invite).id}
      get new_admin_user_url, params: params
      assert_response :success
    end

    def test_create_without_invite
      params = {user: attributes_for(:user)}

      assert_no_difference('User.count') do
        post admin_users_url, params: params
      end
      assert_response :bad_request
    end

    def test_create_with_nonexistent_invite
      params = {
        invite: 'nonexistent invite',
        user: attributes_for(:user)
      }

      assert_no_difference('User.count') do
        post admin_users_url, params: params
      end
      assert_response :bad_request
    end

    def test_create_with_used_invite
      params = {
        invite: create(:user).invite_id,
        user: attributes_for(:user)
      }

      assert_no_difference('User.count') do
        post admin_users_url, params: params
      end
      assert_response :bad_request
    end

    def test_create_with_fresh_invite
      params = {
        invite: create(:invite).id,
        user: attributes_for(:user)
      }

      assert_difference('User.count') do
        post admin_users_url, params: params
      end
      assert_redirected_to login_url
    end

    def test_edit_own_profile
      get edit_admin_user_url(@current_user)
      assert :success
    end

    def test_edit_other_profile
      user = create(:user)
      get edit_admin_user_url(user)
      assert_forbidden
    end

    def test_update_own_profile
      params = {
        user: {
          name: 'Some Other Name'
        }
      }

      patch admin_user_url(@current_user), params: params
      assert_redirected_to admin_users_url
    end

    def test_update_other_profile
      user = create(:user)
      params = {
        user: {
          name: 'Some Other Name'
        }
      }

      patch admin_user_url(user), params: params
      assert_forbidden
    end

    def test_destroy
      user = create(:user)
      assert_no_difference('User.count') do
        delete admin_user_url(user)
      end
      assert_forbidden
    end
  end

  class AdminUsersControllerTest < ActionDispatch::IntegrationTest
    def setup
      @current_user = create(:user, password: 'password')
      @current_user.admin = true
      @current_user.save(validate: false)

      login(email: @current_user.email, password: 'password')
    end

    def test_index
      get admin_users_url
      assert :success
    end

    def test_edit_own_profile
      get edit_admin_user_url(@current_user)
      assert :success
    end

    def test_edit_other_profile
      user = create(:user)
      get edit_admin_user_url(user)
      assert :success
    end

    def test_update_own_profile
      params = {
        user: {
          name: 'Some Other Name'
        }
      }

      patch admin_user_url(@current_user), params: params
      assert_redirected_to admin_users_url
    end

    def test_update_other_profile
      user = create(:user)
      params = {
        user: {
          name: 'Some Other Name'
        }
      }

      patch admin_user_url(user), params: params
      assert_redirected_to admin_users_url
    end

    def test_destroy
      user = create(:user)
      assert_difference('User.count', -1) do
        delete admin_user_url(user)
      end
      assert_redirected_to admin_users_url
    end
  end
end
