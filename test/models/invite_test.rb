# frozen_string_literal: true

require 'test_helper'

class InviteTest < ActiveSupport::TestCase
  def test_that_new_invite_is_fresh
    invite = build(:invite)
    assert !invite.used? && !invite.expired?
    assert invite.fresh?
  end

  def test_fresh_on_used_invite
    invite = build(:invite)
    def invite.used?
      true
    end
    refute invite.fresh?
  end

  def test_fresh_on_expired_invite
    invite = build(:invite)
    def invite.expired?
      true
    end
    refute invite.fresh?
  end

  def test_fresh_on_used_and_expired_invite
    invite = build(:invite)

    def invite.used?
      true
    end

    def invite.expired?
      true
    end

    refute invite.fresh?
  end

  def test_if_invite_created_eleven_hours_ago_is_expired
    invite = create(:invite, created_at: 11.hours.ago)
    refute invite.expired?
  end

  def test_if_invite_created_thirteen_hours_ago_is_expired
    invite = create(:invite, created_at: 13.hours.ago)
    assert invite.expired?
  end

  def test_used_on_persisted_invite
    invite = create(:invite)
    refute invite.used?
  end

  def test_used_on_actually_used_invite
    invite = create(:invite)
    create(:user, invite: invite)
    assert invite.used?
  end
end
