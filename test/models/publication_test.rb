# frozen_string_literal: true

require 'test_helper'

class PublicationTest < ActiveSupport::TestCase
  def test_publication_without_type
    refute build(:article, type: nil).valid?
    refute build(:news, type: '').valid?
  end

  def test_publication_with_invalid_type
    refute build(:statement, type: 'SomeInvalidType').valid?
  end
end
