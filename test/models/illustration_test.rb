# frozen_string_literal: true

require 'test_helper'

class IllustrationTest < ActiveSupport::TestCase
  def test_if_illustration_created_by_factory_is_valid
    illustration = build(:illustration)
    assert illustration.valid?
  end

  def test_that_too_long_caption_is_rejected
    illustration = build(:illustration, caption: 'A' * 141)
    refute illustration.valid?
  end
end
