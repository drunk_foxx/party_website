# frozen_string_literal: true

require 'test_helper'

class StatementTest < ActiveSupport::TestCase
  def test_that_factory_defines_valid_statement
    statement = build(:statement)
    assert statement.valid?
  end

  def test_requirement_for_author_presence
    statement = build(:statement, user_id: nil)
    refute statement.valid?
  end

  def test_statement_without_picture
    statement = build(:statement, picture: nil)
    refute statement.valid?
  end

  def test_if_statement_is_valid_without_title
    statement = build(:statement, title: '')
    refute statement.valid?
  end

  def test_that_body_is_required
    statement = build(:statement, body: '')
    refute statement.valid?
  end

  def test_published_at_presence_validation
    statement = build(:statement, published_at: nil)
    refute statement.valid?
  end

  def test_that_annotation_is_required
    statement = build(:statement, annotation: '')
    refute statement.valid?
  end

  def test_that_too_long_annotation_is_rejected
    statement = build(:statement, annotation: 'A' * 141)
    refute statement.valid?
  end
end
