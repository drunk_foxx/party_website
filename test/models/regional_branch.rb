# frozen_string_literal: true

class RegionalBranchTest < ActiveSupport::TestCase
  def test_that_regional_branch_defined_by_factory_is_valid
    regional_branch = build(:regional_branch)
    assert regional_branch.valid?
  end
end
