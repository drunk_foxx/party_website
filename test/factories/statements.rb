# frozen_string_literal: true

FactoryBot.define do
  factory :statement do
    association :author, factory: :user
    association :picture

    annotation 'ФК ЛПР о непризнании результатов выборов'
    body '18 марта в России прошло голосование, официальная явка составила…'
    published_at { Date.yesterday }
    title 'Выборы 18-го марта'
    type 'Statement'
  end
end
