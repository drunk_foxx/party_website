# frozen_string_literal: true

FactoryBot.define do
  factory :membership_request do
    association :federal_subject, factory: :moscow

    birthdate Date.new(1989, 2, 13)
    full_name 'Дмитрий Сергеевич Цорионов'
    phone '+71234567890'
    residence 'Москва'
  end
end
