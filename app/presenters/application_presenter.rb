# frozen_string_literal: true

require 'delegate'

class ApplicationPresenter < SimpleDelegator
  def initialize(object, context)
    @context = context
    super(object)
  end

  def to_model
    __getobj__
  end

  def class
    __getobj__.class
  end
end
