# frozen_string_literal: true

class Publication < ApplicationRecord
  VALID_TYPES = %w[Article News Statement].freeze

  belongs_to :author, class_name: 'User', foreign_key: 'user_id'
  has_one :picture, as: :picturable, dependent: :destroy

  accepts_nested_attributes_for :picture

  validates :annotation, presence: true, length: {maximum: 140}
  validates :author, presence: true
  validates :body, presence: true
  validates :picture, presence: true
  validates :published_at, presence: true
  validates :title, presence: true
  validates :type, inclusion: {in: VALID_TYPES}

  def self.types
    VALID_TYPES
  end
end
