# frozen_string_literal: true

class Picture < ApplicationRecord
  belongs_to :picturable, polymorphic: true
  has_one_attached :file

  validates :alt, length: {maximum: 140}

  validate :file_is_attached
  validate :attached_file_is_image

  private

  def file_is_attached
    errors.add(:file, :blank) unless file.attached?
  end

  def attached_file_is_image
    return unless file.attached?

    errors.add(:file, :not_an_image) unless file.image?
  end
end
