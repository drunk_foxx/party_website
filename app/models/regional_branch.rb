# frozen_string_literal: true

class RegionalBranch < ApplicationRecord
  belongs_to :federal_subject
end
