# frozen_string_literal: true

module ApplicationHelper
  def time_ago_in_words_or_short_date(time)
    if (Time.now - time) < 24.hours
      "#{time_ago_in_words(time)} #{t('helpers.ago')}"
    else
      l(time.to_date, format: :short)
    end
  end

  def link_to_active_if_current(name, options = {}, html_options = {})
    current_page?(options) &&
      html_options[:class] = format('%1$s %1$s-active', html_options[:class])
    link_to name, options, html_options
  end

  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end

  def markup(content)
    renderer_options = {
      filter_html: true,
      headers_start_from: 2
    }

    renderer = PublicationRenderer.new(renderer_options)

    markdown_options = {
      autolink: true,
      no_intra_emphasis: true,
      strikethrough: true
    }

    Redcarpet::Markdown
      .new(renderer, markdown_options)
      .render(sanitize(content)).html_safe
  end
end
