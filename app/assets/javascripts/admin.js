/*
 *= require rails-ujs
 *= require turbolinks
 *= require yamete
 *
 *= require ./shared/util
 *= require_tree ./shared/buttons
 *
 *= require ./admin/editor
 *= require ./admin/navbar_burger
 */
