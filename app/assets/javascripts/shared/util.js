/* exported Util */

var Util = {
  activate: function (el) {
    el.classList.add("is-active");
  },

  deactivate: function (el) {
    el.classList.remove("is-active");
  },

  hide: function (el) {
    el.classList.add("is-hidden");
  },

  isPrimaryButton: function (event) {
    if (event.buttons) {
      return event.buttons === 1;
    } else if (event.which) {
      return event.which === 1;
    } else {
      return event.button === 0;
    }
  },

  parentWithClass: function findParentWithClass(klass, el) {
    el = el.parentElement;
    if (el.classList.contains(klass) || el === null) {
      return el;
    }
    return findParentWithClass(klass, el);
  },

  selectAll: function (selector, el) {
    if (typeof el === "undefined") {
      el = document;
    }

    return Array.prototype.slice.call(el.querySelectorAll(selector));
  },

  show: function (el) {
    el.classList.remove("is-hidden");
  }
};
