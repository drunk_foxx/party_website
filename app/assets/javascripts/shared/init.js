document.addEventListener("turbolinks:load", function () {
  Util.selectAll("[data-active]").forEach(function (el) {
    Util.activate(el);
    el.removeAttribute("data-active");
  });

  Util.selectAll("[data-hidden]").forEach(function (el) {
    Util.hide(el);
    el.removeAttribute("data-hidden");
  });

  Util.selectAll("[data-inactive]").forEach(function (el) {
    Util.deactivate(el);
    el.removeAttribute("data-inactive");
  });

  Util.selectAll("[data-shown]").forEach(function (el) {
    Util.show(el);
    el.removeAttribute("data-shown");
  });
});
