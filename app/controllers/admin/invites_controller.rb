# frozen_string_literal: true

module Admin
  class InvitesController < BaseController
    before_action :require_admin
    before_action :set_invite, only: %i[show destroy]

    def index
      @invites = Invite.reorder(created_at: :desc).page(params[:page]).per(9)
    end

    def show
    end

    def new
      @invite = Invite.new
    end

    def create
      @invite = Invite.new(invite_params)

      if @invite.save
        redirect_to admin_invite_url(@invite), notice: t('.success')
      else
        render :new
      end
    end

    def destroy
      @invite.destroy
      redirect_to admin_invites_url, notice: t('.success')
    end

    private

    def set_invite
      @invite = Invite.find(params[:id])
    end

    def invite_params
      params.require(:invite).permit(:comment)
    end
  end
end
