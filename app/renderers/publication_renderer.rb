# frozen_string_literal: true

class PublicationRenderer < Redcarpet::Render::HTML
  def initialize(options = {})
    @headers_start_from = options[:headers_start_from] || 1
    super
  end

  def header(text, level)
    level += @headers_start_from - 1

    # Ensure level doesn't exceed h6.
    level = 6 if level > 6

    format('<h%1$d>%2$s</h%1$d>', level, text)
  end
end
