# frozen_string_literal: true

Dir[Rails.root.join('db', 'seeds', '*.yml')].each do |seed|
  model = File.basename(seed, '.yml').classify.constantize
  model.delete_all
  model.create(YAML.safe_load(File.read(seed)))
end

# Create a user to browse /admin in development.
FactoryBot.create(:user) if Rails.env.development?
