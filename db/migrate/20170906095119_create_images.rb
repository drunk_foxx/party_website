# frozen_string_literal: true

class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images, id: :uuid do |t|
      t.string :upload_fingerprint
      t.attachment :upload
      t.string :alt

      t.timestamps
    end

    create_table :images_news, id: false do |t|
      t.belongs_to :image, type: :uuid
      t.belongs_to :news, type: :uuid
    end
  end
end
