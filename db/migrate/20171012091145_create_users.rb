# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users, id: :uuid do |t|
      t.string :name

      # Sorcery Core module fields.
      t.string :email
      t.string :encrypted_password
      t.string :salt

      # Sorcery User Activation module fields.
      t.string :activation_state
      t.string :activation_token
      t.datetime :activation_token_expires_at

      # Sorcery Remember Me module fields.
      t.string :remember_me_token
      t.datetime :remember_me_token_expires_at

      # Sorcery Reset Password module fields.
      t.string :reset_password_token
      t.datetime :reset_password_token_expires_at
      t.datetime :reset_password_email_sent_at

      t.timestamps
    end

    add_index :users, :email
    add_index :users, :activation_token
    add_index :users, :remember_me_token
    add_index :users, :reset_password_token
  end
end
