# frozen_string_literal: true

class ReplaceImageWithPicture < ActiveRecord::Migration[5.2]
  def change
    drop_table :images, id: :uuid do |t|
      t.string :upload_fingerprint
      t.string :upload_file_name
      t.string :upload_content_type
      t.integer :upload_file_size
      t.datetime :upload_updated_at
      t.string :alt
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end

    drop_table :images_publications, id: false do |t|
      t.uuid :image_id
      t.uuid :publication_id
      t.index [:image_id], name: 'index_images_publications_on_image_id'
      t.index [:publication_id], name: 'index_images_publications_on_publication_id'
    end

    create_table :pictures, id: :uuid do |t|
      t.string :alt
      t.references :picturable,
                   type: :uuid,
                   polymorphic: true,
                   index: {unique: true}
      t.timestamps null: true
    end
  end
end
