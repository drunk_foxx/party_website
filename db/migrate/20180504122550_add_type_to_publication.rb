# frozen_string_literal: true

class AddTypeToPublication < ActiveRecord::Migration[5.1]
  def change
    add_column :publications, :type, :string
  end
end
