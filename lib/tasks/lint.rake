# frozen_string_literal: true

namespace :project do
  namespace :lint do
    desc 'Lint just Ruby'
    task :ruby do
      sh 'bin/bundle exec rubocop --safe-auto-correct'
    end

    desc 'Lint only JS'
    task :js do
      files = 'config/webpack/*.js frontend/**/*.{js,jsx}'
      sh "bin/yarn standard --env jest --fix #{files}"
    end
  end

  desc 'Run all linters'
  task lint: %i[lint:ruby lint:js]
end
